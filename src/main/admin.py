from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils.html import format_html
from semantic_admin import SemanticModelAdmin
from semantic_admin.filters import SemanticFilterSet

from .models import Profile
from .models import CustomUser


class InlineFilter(SemanticFilterSet):
    class Meta:
        model = get_user_model()
        fields = ('user_type',)


@admin.register(CustomUser)
class CustomUser(SemanticModelAdmin):
    filter_class = InlineFilter
    date_hierarchy = 'date_joined'
    ordering = ('pk',)
    list_display = ('avatar', 'first_name', 'last_name', 'email', 'phone_number', 'user_type', 'address', )
    list_display_links = ('avatar', )
    search_fields = ('first_name', 'last_name', 'email', 'phone_number', 'user_type', 'address',)
    fieldsets = (
        ('Personal info', {
            'fields': ('phone_number', 'email', 'photo', ),
            'description': 'General info about the student'
        }
        ),
        ('Additional info', {
            'classes': ('collapse',),
            'fields': (('address', 'location'), ('user_type', 'facebook'), ('twitter', 'instagram')),
        })
    )

    def avatar(self, obj):
        if obj.photo:
            photo = f"<img src='{obj.photo.url}' style='width:150px;height:150px;' alt='Avatar'>"
            return format_html(photo)
        return "No avatar"


class ProfileStackedInline(admin.StackedInline):
    model = get_user_model()


@admin.register(Profile)
class ProfileAdmin(SemanticModelAdmin):

    date_hierarchy = 'date_created'
    ordering = ('pk',)
