from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView, PasswordChangeView, \
    PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import TemplateView, CreateView, RedirectView, DetailView, UpdateView


from config import settings
from main.forms import RegistrateForm, LoginForm, UpdateProfileForm, PasswordResetForm, ChangePasswordForm, \
    PasswordResetConfirmForm
from services.register_email_sending.registration_email import send_registration_email
from services.utils.token_creator import TokenCreator


class PageNotFound(LoginRequiredMixin, TemplateView):
    template_name = "main/404.html"
    extra_context = {"title": "Page not found"}


class Forbidden(TemplateView):
    template_name = "main/forbidden.html"
    extra_context = {"title": "Forbidden"}


class IndexView(TemplateView):
    template_name = "main/index.html"
    extra_context = {"title": "Main page"}


class RegisterUser(CreateView):
    form_class = RegistrateForm
    template_name = 'main/register.html'
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(request=self.request, user_instance=self.object)
        return super().form_valid(form)


class PasswordReset(PasswordResetView):
    form_class = PasswordResetForm
    template_name = 'main/password_reset.html'


class PasswordResetDone(PasswordResetDoneView):
    template_name = 'main/password_reset_sent.html'


class PasswordResetConfirm(PasswordResetConfirmView):
    form_class = PasswordResetConfirmForm
    template_name = 'main/password_reset_confirm.html'


class PasswordResetComplete(PasswordResetCompleteView):
    template_name = 'main/password_reset_complete.html'


class PasswordChange(LoginRequiredMixin, PasswordChangeView):
    form_class = ChangePasswordForm
    template_name = 'main/password_change.html'
    success_url = reverse_lazy('index')


class LoginUser(LoginView):
    form_class = LoginForm
    template_name = 'main/login.html'


class LogoutUser(LoginRequiredMixin, LogoutView):
    pass


class ActivateUser(RedirectView):
    url = reverse_lazy('index')

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data")

        if current_user and TokenCreator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)
            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data")


class UserProfile(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = "main/user_profile.html"
    extra_context = {"key_api": settings.LOCATION_FIELD['provider.google.api_key'],
                     "title": 'Profile'}
    raise_exception = True


class UpdateUserProfile(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    template_name = "main/update_user_profile.html"
    form_class = UpdateProfileForm
    success_url = reverse_lazy('index')
    raise_exception = True
