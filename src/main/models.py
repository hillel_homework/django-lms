from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from location_field.models.plain import PlainLocationField
from phonenumber_field.modelfields import PhoneNumberField

from main.managers import CustomUserManager

user_type = (('Teacher', 'Teacher'), ('Mentor', 'Mentor'), ('Student', 'Student'))


class CustomUser(AbstractBaseUser, PermissionsMixin):
    phone_number = PhoneNumberField(_("phone number"), blank=True, null=True, help_text="Contact phone number")
    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    email = models.EmailField(_("email address"), blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    location = PlainLocationField(based_fields=['address'], zoom=7, null=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    photo = models.ImageField(default='profiles_avatars/empty_avatar.png', null=True,
                              blank=True, upload_to="profiles_avatars/%Y/%m/%d/")

    user_type = models.CharField(max_length=50, choices=user_type, default="student")
    facebook = models.CharField(null=True, blank=True, max_length=255)
    instagram = models.CharField(null=True, blank=True, max_length=255)
    twitter = models.CharField(null=True, blank=True, max_length=255)
    username = models.CharField(null=True, blank=True, max_length=255)

    objects = CustomUserManager()

    USERNAME_FIELD = "phone_number"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("custom_user")
        verbose_name_plural = _("custom_users")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def __str__(self):
        return str(self.phone_number) + " " + str(self.first_name) + str(self.last_name)


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, primary_key=True,)
    date_created = models.DateTimeField(auto_now_add=True, null=True, editable=False)

    def __str__(self):
        return str(self.user)
