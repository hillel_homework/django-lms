from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordResetForm, PasswordChangeForm, \
    SetPasswordForm
from django.core.exceptions import ValidationError


class RegistrateForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        self.phone_number_email_exist = False
        super().__init__(*args, **kwargs)

    class Meta:
        model = get_user_model()
        fields = ("phone_number", "first_name", "last_name", "email", "address",
                  "user_type")

        widgets = {
            "first_name": forms.TextInput(attrs={'class': 'form-control'}),
            "last_name": forms.TextInput(attrs={'class': 'form-control'}),
            "email": forms.EmailInput(attrs={'class': 'form-control'}),
            "address": forms.TextInput(attrs={'class': 'form-control'}),
            "user_type": forms.Select(attrs={'class': 'form-select'}, ),
        }
    phone_number = forms.CharField(label="Phone number",
                                   widget=forms.TextInput(attrs={'class': 'form-control'},),
                                   required=False)
    password1 = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label="Confirm password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_phone_number(self):
        phone_number = self.cleaned_data["phone_number"]
        phones_exist = get_user_model().objects.filter(phone_number=phone_number).exists()
        if not phones_exist:
            return phone_number
        raise ValidationError("Entered phone number already exists")

    def clean_email(self):
        email = self.cleaned_data["email"]
        emails_exist = get_user_model().objects.filter(email=email)
        if len(emails_exist) == 0:
            return email
        raise ValidationError("Entered email already exists")

    def clean(self):
        super().clean()
        if 'email' not in self.cleaned_data or 'phone_number' not in self.cleaned_data:
            pass
        else:
            email = self.cleaned_data["email"]
            phone_number = self.cleaned_data["phone_number"]
            print(self.cleaned_data)
            if not email and not phone_number:
                raise ValidationError("Please enter either phone number or email or both")


class PasswordResetForm(PasswordResetForm):

    class Meta:
        model = get_user_model()
        fields = "__all__"

    email = forms.CharField(label="Email", widget=forms.EmailInput(attrs={'class': 'form-control'}))


class PasswordResetConfirmForm(SetPasswordForm):

    new_password1 = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password2 = forms.CharField(label="Confirm password",
                                    widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class ChangePasswordForm(PasswordChangeForm):

    class Meta:
        model = get_user_model()

    old_password = forms.CharField(label="Old password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password1 = forms.CharField(label="New password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password2 = forms.CharField(label="Confirm new password",
                                    widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class LoginForm(AuthenticationForm):

    class Meta:
        model = get_user_model()

    username = forms.CharField(label="Phone number/Email", widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class UpdateProfileForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ("first_name", "last_name", "email", "address", "phone_number", "photo",
                  "user_type", "facebook", "instagram", "twitter")

        widgets = {
            "phone_number": forms.TextInput(attrs={'class': 'form-control'}),
            "first_name": forms.TextInput(attrs={'class': 'form-control'}),
            "last_name": forms.TextInput(attrs={'class': 'form-control'}),
            "email": forms.EmailInput(attrs={'class': 'form-control'}),
            "address": forms.TextInput(attrs={'class': 'form-control'}),
            "photo": forms.ClearableFileInput(attrs={'class': 'form-control'}, ),
            "user_type": forms.Select(attrs={'class': 'form-select'}, ),
            "facebook": forms.TextInput(attrs={'class': 'form-control'}),
            "instagram": forms.TextInput(attrs={'class': 'form-control'}),
            "twitter": forms.TextInput(attrs={'class': 'form-control'}),
        }

    def clean_phone_number(self):
        phone_number = self.cleaned_data["phone_number"]
        phones_exist = get_user_model().objects.filter(phone_number=phone_number)
        if len(phones_exist) == 0:
            return phone_number
        raise ValidationError("Entered phone number already exists")

    def clean_email(self):
        email = self.cleaned_data["email"]
        emails_exist = get_user_model().objects.filter(email=email)
        if len(emails_exist) == 0:
            return email
        raise ValidationError("Entered email already exists")
