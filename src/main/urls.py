from django.urls import path
from main.views import IndexView, RegisterUser, LoginUser, LogoutUser, \
    ActivateUser, UserProfile, UpdateUserProfile, PasswordReset, PasswordChange, \
    PasswordResetDone, PasswordResetConfirm, PasswordResetComplete

urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('register/', RegisterUser.as_view(), name="register"),
    path('login/', LoginUser.as_view(), name="login"),
    path('logout/', LogoutUser.as_view(), name="logout"),
    path('activate/<str:uuid64>/<str:token>/', ActivateUser.as_view(), name="activate_user"),
    path('profile/<int:pk>/', UserProfile.as_view(), name="user_profile"),
    path('profile/update/<int:pk>/', UpdateUserProfile.as_view(), name="update_user_profile"),
    path('change_password/<int:pk>/', PasswordChange.as_view(), name="change_password"),
    path('reset_password/', PasswordReset.as_view(), name="reset_password"),
    path('reset_password_sent/', PasswordResetDone.as_view(), name="password_reset_done"),
    path('reset_password/<uidb64>/<str:token>/', PasswordResetConfirm.as_view(), name="password_reset_confirm"),
    path('reset_password_complete/', PasswordResetComplete.as_view(), name="password_reset_complete"),
]
