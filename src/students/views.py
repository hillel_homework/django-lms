import pandas as pd
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from django.db.models import Q
from django.http import HttpResponse
from django.core.exceptions import BadRequest
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from faker import Faker
from webargs import fields
from webargs.djangoparser import use_args

from students.forms import StudentForm
from students.models import Student


@login_required
def generate_students(request):
    if (request.GET.get('count', "")).isdigit():
        count = int(request.GET['count'])
        fake_inst = Faker("UK")
        # A dict for creating fake data
        columns_data = {"first_name": fake_inst.first_name, "last_name": fake_inst.last_name,
                        "email": fake_inst.email, "birthday": None, "password": fake_inst.password}
        # Fake data creation
        for column_name in columns_data:
            columns_data[column_name] = [columns_data[column_name]() for _ in range(count)] \
                if column_name != 'birthday' \
                else [fake_inst.date_of_birth(minimum_age=18, maximum_age=45) for _ in range(count)]

        students_table = pd.DataFrame(columns_data).to_html()
        return HttpResponse(f"<p>{students_table}</p>")
    else:
        raise BadRequest


class GetStudents(LoginRequiredMixin, ListView):
    model = Student
    template_name = "students/students.html"
    context_object_name = "students"
    extra_context = {'title': 'Students'}
    raise_exception = True

    @use_args({"first_name": fields.Str(required=False),
               "last_name": fields.Str(required=False),
               "email": fields.Str(required=False),
               "search": fields.Str(required=False)}, location="query")
    def get_queryset(self, parameters):
        search_fields = ["first_name", "last_name", "email"]
        students_queryset = Student.objects.all()
        for param_name, param_value in parameters.items():
            if param_value:
                # Search is carried by all search_fields
                if param_name == "search":
                    or_filter = Q()
                    for field in search_fields:
                        or_filter |= Q(**{f"{field}__icontains": param_value})
                    students_queryset = students_queryset.filter(or_filter)
                # Search is carried out by fields that were filled, except "search" field
                else:
                    students_queryset = students_queryset.filter(**{f"{param_name}__icontains": param_value})
        return students_queryset


class CreateStudent(LoginRequiredMixin, CreateView):
    model = Student
    template_name = "students/students_create.html"
    form_class = StudentForm
    success_url = reverse_lazy('students')
    raise_exception = True


class UpdateStudent(LoginRequiredMixin, UpdateView):
    model = Student
    template_name = "students/students_update.html"
    form_class = StudentForm
    success_url = reverse_lazy('students')
    raise_exception = True


class DeleteStudent(LoginRequiredMixin, DeleteView):
    model = Student
    template_name = "students/students_delete.html"
    success_url = reverse_lazy('students')
    raise_exception = True
