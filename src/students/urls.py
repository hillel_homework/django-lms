from django.urls import path

from students.views import generate_students, CreateStudent, UpdateStudent, DeleteStudent, GetStudents

urlpatterns = [
    path("generate_students/", generate_students, name="generate_students"),
    path('', GetStudents.as_view(), name="students"),
    path('create/', CreateStudent.as_view(), name="create_student"),
    path('update/<uuid:pk>/', UpdateStudent.as_view(), name="update_student"),
    path('delete/<uuid:pk>/', DeleteStudent.as_view(), name="delete_student"),
]
