from uuid import uuid4
from datetime import datetime

from django.db import models
from faker import Faker
from random import randint


class Student(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    email = models.EmailField(max_length=100, null=True)
    birth_date = models.DateField(null=True, default="2000-12-31")
    grade = models.IntegerField(default=0, editable=False)
    done_hw = models.IntegerField(default=0, editable=False)
    group = models.ForeignKey('groups.Group', on_delete=models.SET_NULL, null=True, blank=True)
    avatar = models.ImageField(null=True, blank=True, upload_to="images/%Y/%m/%d/")
    cv = models.FileField(null=True, blank=True, upload_to="pdfs/")

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email}"

    def age(self):
        return datetime.now().year - self.birth_date.year

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            grade = randint(1, 100)
            if 1 < grade < 50:
                done_hw = randint(1, 8)
            elif 50 < grade < 75:
                done_hw = randint(9, 12)
            else:
                done_hw = randint(13, 16)
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(
                    start_date='-30y', end_date='-18y'
                ),
                grade=grade,
                done_hw=done_hw,)
