from datetime import datetime
from django.core.exceptions import ValidationError
from django import forms

from groups.models import Group
from students.models import Student


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ("first_name", "last_name", "email", "birth_date", "group", "avatar", "cv")

        widgets = {
            "first_name": forms.TextInput(attrs={'class': 'form-control'}),
            "last_name": forms.TextInput(attrs={'class': 'form-control'}),
            "email": forms.EmailInput(attrs={'class': 'form-control'}),
            "birth_date": forms.SelectDateWidget(attrs={"class": "form-select mb-3"},
                                                 years=range(1965, datetime.now().year - 16)),
            "avatar": forms.ClearableFileInput(attrs={'class': 'form-control'}, ),
            "cv": forms.ClearableFileInput(attrs={'class': 'form-control', 'label': 'Curriculum vitae'},),
        }

    # cv = forms.FileField(label="Curriculum vitae",
    #                      widget=forms.ClearableFileInput(attrs={'class': 'form-control'}), )
    group = forms.ModelChoiceField(queryset=Group.objects.all(),
                                   empty_label="Choose a group id",
                                   widget=forms.Select(attrs={"class": "form-select"}))

    def clean_first_name(self):
        first_name = self.cleaned_data["first_name"]
        if list(filter(str.isdigit, first_name)):
            raise ValidationError("Name can not has a number in it")
        return first_name

    def clean_last_name(self):
        last_name = self.cleaned_data["last_name"]
        if list(filter(str.isdigit, last_name)):
            raise ValidationError("Last name can not has a number in it")
        return last_name

    def clean_birth_date(self):
        birth_date = self.cleaned_data["birth_date"]
        if 1965 <= birth_date.year < datetime.now().year - 16:
            return birth_date
        raise ValidationError("Incorrect year of birth")
