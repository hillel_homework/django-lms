import pandas as pd

from django.core.management.base import BaseCommand
from faker import Faker


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help='Q-ty of students to be generated')

    @staticmethod
    def generate_students_term(count):
        fake_inst = Faker("UK")
        # A dict for creating fake data
        columns_data = {"first_name": fake_inst.first_name, "last_name": fake_inst.last_name,
                        "email": fake_inst.email, "birthday": None, "password": fake_inst.password}
        # Fake data creation
        for column_name in columns_data:
            columns_data[column_name] = [columns_data[column_name]() for _ in range(count)] \
                if column_name != 'birthday'\
                else [fake_inst.date_of_birth(minimum_age=18, maximum_age=45) for _ in range(count)]
        print(pd.DataFrame(columns_data))

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        self.generate_students_term(count)
