from django.contrib import admin
from django.utils.html import format_html
from semantic_admin import SemanticModelAdmin
from semantic_admin.filters import SemanticFilterSet

from .models import Student


class GradeListFilter(admin.SimpleListFilter):
    title = 'Grade'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'student_grade'

    def lookups(self, request, model_admin):

        return (
            ('Excellent', 'Excellent'),
            ('Very good', 'Very good'),
            ('Good', 'Good'),
            ('Weak', 'Weak'),
            ('Unacceptable', 'Unacceptable'),
        )

    def queryset(self, request, queryset):

        if self.value() == 'Excellent':
            return queryset.filter(
                grade__lte=100,
                grade__gte=80,
            )
        if self.value() == 'Very good':
            return queryset.filter(
                grade__lte=80,
                grade__gte=60,
            )
        if self.value() == 'Good':
            return queryset.filter(
                grade__lte=60,
                grade__gte=40,
            )
        if self.value() == 'Weak':
            return queryset.filter(
                grade__lte=40,
                grade__gte=20,
            )
        if self.value() == 'Unacceptable':
            return queryset.filter(
                grade__lte=20,
                grade__gte=0,
            )


class GroupListFilter(admin.SimpleListFilter):
    title = 'Faculty'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'student_group_id'

    def lookups(self, request, model_admin):

        return (
            ('Python', 'Python'),
            ('FrontEnd', 'FrontEnd'),
            ('Java', 'Java'),
            ('PHP', 'PHP'),
            ('C#', 'C#'),
        )

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(
                group_id__faculty=f'{self.value()}'
            )


class InlineFilter(SemanticFilterSet):
    class Meta:
        model = Student
        fields = ('last_name', 'email', )


@admin.register(Student)
class StudentAdmin(SemanticModelAdmin):
    filter_class = InlineFilter
    list_display = ('photo', 'first_name', 'last_name', 'email', 'group', 'grade',)
    list_display_links = ('photo', )
    search_fields = ('first_name', 'last_name', 'email')
    list_filter = ('group', GradeListFilter, GroupListFilter)
    date_hierarchy = 'birth_date'
    readonly_fields = ('grade', 'done_hw')
    fieldsets = (
        ('Personal info', {
            'fields': ('avatar', ('first_name', 'last_name'), ('email', 'birth_date'), ),
            'description': 'General info about the student'
        }
        ),
        ('Additional info', {
            'classes': ('collapse', ),
            'fields': (('group', 'cv'), ('grade', 'done_hw')),
        })
    )

    def photo(self, obj):
        print(obj.avatar)
        if obj.avatar:
            avatar = f"<img src='{obj.avatar.url}' style='width:150px;height:150px;' alt='Avatar'>"
            return format_html(avatar)
        return "No avatar"
