from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from webargs import fields
from webargs.djangoparser import use_args

from groups.models import Group
from groups.forms import GroupForm


class GetGroups(LoginRequiredMixin, ListView):
    model = Group
    template_name = "groups/groups.html"
    context_object_name = "groups"
    extra_context = {'title': 'Groups'}
    raise_exception = True

    @use_args({"course": fields.Str(required=False),
               "faculty": fields.Str(required=False),
               "group_id": fields.Int(load_default=None),
               "search": fields.Str(required=False)}, location="query")
    def get_queryset(self, parameters):
        groups_queryset = Group.objects.all()

        search_fields = ["course", "faculty", "group_id"]

        for param_name, param_value in parameters.items():
            if param_value:
                # Search is carried by all search_fields
                if param_name == "search":
                    or_filter = Q()
                    for field in search_fields:
                        or_filter |= Q(**{f"{field}__icontains": param_value})
                    groups_queryset = groups_queryset.filter(or_filter)
                # Search is carried out by fields that were filled, except "search" field
                else:
                    groups_queryset = groups_queryset.filter(**{f"{param_name}__icontains": param_value})
        return groups_queryset


class CreateGroup(LoginRequiredMixin, CreateView):
    model = Group
    template_name = "groups/group_create.html"
    form_class = GroupForm
    success_url = reverse_lazy('groups')
    raise_exception = True


class UpdateGroup(LoginRequiredMixin, UpdateView):
    model = Group
    template_name = "groups/group_update.html"
    form_class = GroupForm
    success_url = reverse_lazy('groups')
    raise_exception = True


class GroupInfo(LoginRequiredMixin, DetailView):
    model = Group
    template_name = "groups/group_info.html"
    raise_exception = True


class DeleteGroup(LoginRequiredMixin, DeleteView):
    model = Group
    template_name = "groups/group_delete.html"
    success_url = reverse_lazy('groups')
    raise_exception = True
