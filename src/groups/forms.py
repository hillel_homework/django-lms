from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from django import forms
from groups.models import Group


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ("faculty", "course", "course_duration_weeks", "lsn_in_course", "start_date")

        widgets = {
            "faculty": forms.Select(attrs={"class": "form-select"}),
            "course": forms.Select(attrs={"class": "form-select"}),
            "email": forms.EmailInput(attrs={'class': 'form-control'}),
            "lsn_in_course": forms.Select(attrs={"class": "form-select"}),
            "course_duration_weeks": forms.Select(attrs={"class": "form-select"}),
            "start_date": forms.SelectDateWidget(attrs={"class": "form-select mb-3"},
                                                 years=range(datetime.now().year, datetime.now().year + 2)),
        }

    def clean_start_date(self):
        start_date = self.cleaned_data['start_date']
        if start_date > datetime.date(datetime.now() - timedelta(weeks=4)):
            return start_date
        raise ValidationError("You are trying to assign invalid date")
