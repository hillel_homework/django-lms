from django.contrib import admin
from semantic_admin import SemanticModelAdmin, SemanticTabularInline, SemanticStackedInline
from semantic_admin.filters import SemanticFilterSet

from students.models import Student
from teachers.models import Teacher
from .models import Group


class InlineFilter(SemanticFilterSet):
    class Meta:
        model = Group
        fields = ('faculty', 'course', )


class StudentStackedInline(SemanticStackedInline):
    model = Student


class TeacherTabularInline(SemanticTabularInline):
    model = Teacher.linked_group_id.through


@admin.register(Group)
class GroupAdmin(SemanticModelAdmin):
    filter_class = InlineFilter
    inlines = [StudentStackedInline, TeacherTabularInline]
    date_hierarchy = 'start_date'
    ordering = ('group_id', )
    list_display = ('group_id', 'faculty', 'course', 'start_date', )
    list_display_links = ('faculty', 'group_id')
    search_fields = ('faculty', 'course')

    fieldsets = (
        ('General info', {
            'fields': (('faculty', 'course'), 'start_date', ),
            'description': 'General info about the student'
        }
        ),
        ('Additional info', {
            'classes': ('collapse', ),
            'fields': (('lsn_in_course', 'course_duration_weeks'), ),
        })
    )
