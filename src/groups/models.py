import datetime

from django.db import models
from faker import Faker
from random import randint

fake = Faker()


class Group(models.Model):
    faculties = (('Python', 'Python'), ('FrontEnd', 'FrontEnd'), ('Java', 'Java'), ('PHP', 'PHP'), ('C#', 'C#'))
    courses = (('basic', 'basic'), ('advance', 'advance'))

    group_id = models.AutoField(primary_key=True)
    faculty = models.CharField(max_length=50, choices=faculties, default="")
    course = models.CharField(max_length=50, choices=courses, default="")
    start_date = models.DateField(null=True, default="2000-12-31")
    course_duration_weeks = models.IntegerField(choices=((8, 8), (16, 16)), default=0)
    lsn_in_course = models.IntegerField(choices=((16, 16), (32, 32)), default=0)

    def __str__(self):
        return f"{self.group_id}"

    @classmethod
    def generate_group(cls, count=1):
        faculties = ['Python', 'FrontEnd', 'Java', 'PHP', 'C#']
        courses = ['basic', 'advance']
        for _ in range(count):
            course = courses[randint(0, 1)]
            if course == 'basic':
                course_duration_weeks = 8
                lsn_in_course = 16
            else:
                course_duration_weeks = 16
                lsn_in_course = 32

            cls.objects.create(
                course=course,
                course_duration_weeks=course_duration_weeks,
                lsn_in_course=lsn_in_course,
                start_date=fake.date_between(datetime.date(2022, 1, 1), datetime.date.today()),
                faculty=faculties[randint(0, len(faculties) - 1)],
            )
