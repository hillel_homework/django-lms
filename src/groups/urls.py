from django.urls import path

from groups.views import GetGroups, CreateGroup, UpdateGroup, DeleteGroup, GroupInfo

urlpatterns = [
    path('', GetGroups.as_view(), name="groups"),
    path('create/', CreateGroup.as_view(), name="create_group"),
    path('update/<int:pk>/', UpdateGroup.as_view(), name="update_group"),
    path('delete/<int:pk>/', DeleteGroup.as_view(), name="delete_group"),
    path('group_info/<int:pk>/', GroupInfo.as_view(), name="group_info")
]
