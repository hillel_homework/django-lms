from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from semantic_admin import SemanticModelAdmin
from semantic_admin.filters import SemanticFilterSet

from .models import Teacher


class TeacherListFilterByCourse(admin.SimpleListFilter):
    title = 'Faculty'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'teacher_linked_group_id'

    def lookups(self, request, model_admin):

        return (
            ('Python', 'Python'),
            ('FrontEnd', 'FrontEnd'),
            ('Java', 'Java'),
            ('PHP', 'PHP'),
            ('C#', 'C#'),
        )

    def queryset(self, request, queryset):

        if self.value() == 'Python':
            return queryset.filter(
                linked_group_id__faculty='Python'
            )
        if self.value() == 'FrontEnd':
            return queryset.filter(
                linked_group_id__faculty='FrontEnd'
            )
        if self.value() == 'Java':
            return queryset.filter(
                linked_group_id__faculty='Java'
            )
        if self.value() == 'PHP':
            return queryset.filter(
                linked_group_id__faculty='PHP'
            )
        if self.value() == 'C#':
            return queryset.filter(
                linked_group_id__faculty='C#'
            )


class InlineFilter(SemanticFilterSet):
    class Meta:
        model = Teacher
        fields = ('degree',)


@admin.register(Teacher)
class TeacherAdmin(SemanticModelAdmin):
    filter_class = InlineFilter
    date_hierarchy = 'modified'
    ordering = ('first_name', 'last_name', )
    list_display = ('photo', 'first_name', 'last_name', 'email', 'degree', 'group_count', 'linked_group')
    list_display_links = ('photo', )
    search_fields = ('first_name', 'pk', 'email', )
    list_filter = (TeacherListFilterByCourse, )

    def group_count(self, obj):
        return obj.linked_group_id.count()

    def linked_group(self, obj):
        if obj.linked_group_id:
            groups = obj.linked_group_id.all()
            links_on_groups = []
            for group in groups:
                links_on_groups.append(
                    f"<a class='ui green fluid button' href='{reverse('admin:groups_group_change', args=(group.pk,))}'>"
                    f"Group {group.pk}</a>"
                )
            return format_html('<br /><br />'.join(links_on_groups))
        return "<button class='ui green fluid button' type='button'>No group</button>"

    def photo(self, obj):
        if obj.avatar:
            avatar = f"<img src='{obj.avatar.url}' style='width:150px;height:150px;' alt='Avatar'>"
            return format_html(avatar)
        return "No avatar"

    fieldsets = (
        ('Personal info', {
            'fields': (('first_name', 'last_name'), ('email', 'degree')),
            'description': 'General info about the student'
        }
        ),
        ('Additional info', {
            'classes': ('collapse',),
            'fields': (('linked_group_id', 'checked_hw'), 'avatar'),
        })
    )
