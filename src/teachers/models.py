from django.db import models
from faker import Faker
from random import randint

degree = (("professor", "professor"), ("master", "master"), ("specialist", "specialist"))


class Teacher(models.Model):
    first_name = models.CharField(max_length=40, default="")
    last_name = models.CharField(max_length=40, default="")
    email = models.EmailField(max_length=100, default="")
    degree = models.CharField(max_length=40, choices=degree, default="")
    linked_group_id = models.ManyToManyField(to='groups.Group', blank=True, null=True)
    checked_hw = models.IntegerField(default=0)
    modified = models.DateTimeField(auto_now=True)
    avatar = models.ImageField(null=True, blank=True, upload_to="images/%Y/%m/%d/")

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email}"

    @classmethod
    def generate_teacher(cls, count=1):
        faker = Faker()
        degree = ["professor", "master", "specialist"]
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                degree=degree[randint(0, 2)],
                checked_hw=randint(1, 100),
            )
