from django.core.exceptions import ValidationError
from django import forms

from teachers.models import Teacher


class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = ("first_name", "last_name", "email", "degree", "linked_group_id", "avatar")

        widgets = {
            "first_name": forms.TextInput(attrs={'class': 'form-control'}),
            "last_name": forms.TextInput(attrs={'class': 'form-control'}),
            "email": forms.EmailInput(attrs={'class': 'form-control'}),
            "degree": forms.Select(attrs={'class': 'form-select'},),
            "linked_group_id": forms.CheckboxSelectMultiple(),
            "avatar": forms.ClearableFileInput(attrs={'class': 'form-control'},),
        }

    def clean_first_name(self):
        first_name = self.cleaned_data["first_name"]
        if list(filter(str.isdigit, first_name)):
            raise ValidationError("Name can not have a number in it")
        return first_name

    def clean_last_name(self):
        last_name = self.cleaned_data["last_name"]
        if list(filter(str.isdigit, last_name)):
            raise ValidationError("Last name can not have a number in it")
        return last_name
