from django.urls import path
from teachers.views import GetTeachers, CreateTeacher, UpdateTeacher, DeleteTeacher

urlpatterns = [
    path('', GetTeachers.as_view(), name="teachers"),
    path('create/', CreateTeacher.as_view(), name="create_teacher"),
    path('update/<int:pk>/', UpdateTeacher.as_view(), name="update_teacher"),
    path('delete/<int:pk>/', DeleteTeacher.as_view(), name="delete_teacher"),
]
