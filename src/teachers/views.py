from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from webargs import fields
from webargs.djangoparser import use_args

from teachers.forms import TeacherForm
from teachers.models import Teacher


class GetTeachers(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = "teachers/teachers.html"
    context_object_name = "teachers"
    extra_context = {'title': 'Teachers'}
    raise_exception = True

    @use_args({"first_name": fields.Str(required=False),
               "last_name": fields.Str(required=False),
               "email": fields.Str(required=False),
               "search": fields.Str(required=False)}, location="query")
    def get_queryset(self, parameters):
        teachers_queryset = Teacher.objects.prefetch_related('linked_group_id')
        search_fields = ["first_name", "last_name", "email"]
        for param_name, param_value in parameters.items():
            if param_value:
                # Search is carried by all search_fields
                if param_name == "search":
                    or_filter = Q()
                    for field in search_fields:
                        or_filter |= Q(**{f"{field}__icontains": param_value})
                    teachers_queryset = teachers_queryset.filter(or_filter)
                # Search is carried out by fields that were filled, except "search" field
                else:
                    teachers_queryset = teachers_queryset.filter(**{f"{param_name}__icontains": param_value})
        return teachers_queryset


class CreateTeacher(LoginRequiredMixin, CreateView):
    model = Teacher
    template_name = "teachers/teacher_create.html"
    form_class = TeacherForm
    success_url = reverse_lazy('teachers')
    raise_exception = True


class UpdateTeacher(LoginRequiredMixin, UpdateView):
    model = Teacher
    template_name = "teachers/teacher_update.html"
    form_class = TeacherForm
    success_url = reverse_lazy('teachers')
    raise_exception = True


class DeleteTeacher(LoginRequiredMixin, DeleteView):
    model = Teacher
    template_name = "teachers/teacher_delete.html"
    success_url = reverse_lazy('teachers')
    raise_exception = True
